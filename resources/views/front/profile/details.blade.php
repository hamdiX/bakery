@extends('front.layouts.master')

@section('content')

    <div class="container">
    <h2 class="mt-5">صفحة تفاصيل الطلب رقم # {{$order->id}} </h2>
    <hr>

    <div class="row">

        <div class="col-md-12">
            <h4 class="title"></h4>
            <div class="content table-responsive table-full-width">
                <table class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th colspan="7">تفاصيل الطلب</th>
                    </tr>
                    <tr>
                        <th>رقم الطلب</th>
                        <th>تاريخ الطلب</th>
                        <th>العنوان</th>
                        <th>حالة الطلب</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>{{ $order->id }}</td>
                        <td>{{ $order->date }}</td>
                        <td>{{ $order->address }}</td>
                        <td>
                            @if ($order->status)
                                <span class="badge badge-success">تم التأكيد</span>
                            @else
                                <span class="badge badge-warning">فى انتظار التأكيد</span>
                            @endif
                        </td>

                    </tbody>
                </table>
            </div>
        </div>
        <div class="col-md-6">

            <h4 class="title">تفاصيل العميل</h4>
            <hr>
            <div class="content table-responsive table-full-width">
                <table class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>رقم العميل</th>
                        <td>{{ $order->user->id }}</td>
                    </tr>
                    <tr>
                        <th>الاسم</th>
                        <td>{{ $order->user->name }}</td>
                    </tr>
                    <tr>
                        <th>البريد الألكترونى</th>
                        <td>{{ $order->user->email }}</td>
                    </tr>
                    <tr>
                        <th>تاريخ التسجيل</th>
                        <td>{{ $order->user->created_at->diffForHumans() }}</td>
                    </tr>

                    </thead>
                </table>
            </div>
        </div>
        <div class="col-md-6">

            <h4 class="title">تفاصيل المنتجات</h4>
            <hr>
            <div class="content table-responsive table-full-width">
                <table class="table table-bordered table-striped">
                    <tr>
                        <th>رقم الطلب</th>
                        <th>اسم المنتج</th>
                        <th>السعر</th>
                        <th>الكمية</th>
                        <th>الصورة</th>
                    </tr>
                    <tr>
                        <td>{{ $order->id }}</td>
                        <td>
                            @foreach ($order->products as $product)
                                <table class="table table-bordered">
                                    <tr>
                                        <td>{{ $product->name }}</td>
                                    </tr>
                                </table>
                            @endforeach
                        </td>

                        <td>
                            @foreach ($order->orderItems as $item)
                                <table class="table  table-bordered">
                                    <tr>
                                        <td>{{ $item->price }}</td>
                                    </tr>
                                </table>
                            @endforeach
                        </td>

                        <td>
                            @foreach ($order->orderItems as $item)
                                <table class="table table-bordered">
                                    <tr>
                                        <td>{{ $item->quantity }}</td>
                                    </tr>
                                </table>
                            @endforeach
                        </td>

                        <td>
                            @foreach ($order->products as $product)
                                <table class="table table-bordered">
                                    <tr>
                                        <td><img src="{{ url('uploads') . '/' . $product->image }}" alt="" style="width: 2em"></td>
                                    </tr>
                                </table>
                            @endforeach
                        </td>
                    </tr>

                </table>

            </div>
        </div>
    </div>
    </div>

@endsection
