@extends('front.layouts.master')

@section('content')
    <div class="container">
        <h2 class="mt-5">الصفحة الشخصية</h2>
        <table class="table table-bordered">
            <thead>
            <tr>
                <th colspan="2">البيانات الشخصية <i class="fa fa-cogs"></i></th>
            </tr>
            </thead>
            <tr>
                <th>#</th>
                <td>{{ $user->id }}</td>
            </tr>
            <tr>
                <th>الاســم</th>
                <td>{{ $user->name}}</td>
            </tr>
            <tr>
                <th>البريد الألكترونى</th>
                <td>{{ $user->email }}</td>
            </tr>
            <tr>
                <th>تاريخ التسجيل</th>
                <td>{{ $user->created_at}}</td>
            </tr>
        </table>


        <h4 class="title">قائمــة الطلبات</h4>
        <hr>
        <div class="content table-responsive table-full-width">
            <table class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>رقم الطلب</th>
                    <th>المنتج</th>
                    <th>الكمية</th>
                    <th>السعر الكلى</th>
                    <th>حالة الطلب</th>
                    <th>العمليات</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    @foreach ($user->order as $order)
                        <td>{{ $order->id }}</td>
                        <td>
                            @foreach ($order->products as $item)
                                <table class="table table-bordered">
                                    <tr>
                                        <td>{{ $item->name }}</td>
                                    </tr>
                                </table>
                            @endforeach
                        </td>

                        <td>
                            @foreach ($order->orderItems as $item)
                                <table class="table table-bordered">
                                    <tr>
                                        <td>{{ $item->quantity }}</td>
                                    </tr>
                                </table>
                            @endforeach
                        </td>

                        <td>
                            @foreach ($order->orderItems as $item)
                                <table class="table">
                                    <tr>
                                        <td>${{ $item->price }}</td>
                                    </tr>
                                </table>
                            @endforeach
                        </td>

                        <td>
                            @if ($order->status)
                                <span class="badge badge-success">تأكيد</span>
                            @else
                                <span class="badge badge-warning">انتظار</span>
                            @endif
                        </td>
                        <td>
                            <a href="{{ url('/user/order') . '/' . $order->id }}" class="btn btn-outline-dark btn-sm">تفاصيل</a>
                        </td>
                </tr>
                @endforeach


                </tbody>
            </table>

        </div>
    </div>
@endsection
