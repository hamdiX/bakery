<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
        <a class="navbar-brand" href="{{ url('/') }}">
            <img src="{{asset('assets/img/logo.png')}}" height="40px" width="50px">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive"
                aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav" style="margin-right:auto !important;">
                <li class="nav-item">
                    <a class="nav-link" href="/cart">
                        @if (Cart::instance('default')->count() > 0)
                            <label class="badge badge-danger rounded-1" style="border-radius: 20px">
                                {{ Cart::instance('default')->count() }}
                            </label>
                        @endif
                            <i class="fa fa-shopping-cart"></i>
                    </a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-item nav-link dropdown-toggle mr-md-2" href="#" id="bd-versions"
                       data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-user"></i> {{ auth()->check() ? auth()->user()->name : 'حســابــي' }}
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="bd-versions">
                        @if (!auth()->check())
                            <a class="dropdown-item " href="{{  url('user/login') }}">تسجيل الدخول</a>
                            <a class="dropdown-item" href="{{  url('user/register') }}"> حساب جديد</a>
                        @else
                            <a class="dropdown-item" href="{{  url('user/profile') }}"><i class="fa fa-user"></i> الصفحة الشخصية</a>
                            <hr>
                            <a class="dropdown-item" href="{{  url('user/logout') }}"><i class="fa fa-lock"></i> تسجيل الخروج</a>
                        @endif
                    </div>
                </li>
            </ul>
        </div>
    </div>
</nav>
