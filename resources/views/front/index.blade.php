@extends('front.layouts.master')

@section('style')
<style>
    .card {
        box-shadow: 0 0 12px 2px darkgrey !important;
    }
    .card-text {
        text-align: right;
        padding: 23px;
    }

    .hover-image:hover + .card-text {
        display: block !important;
    }
</style>
@endsection
@section('content')
    <!-- Jumbotron Header -->
    <header class="jumbotron">
        <h5 class="display-3"><strong>مرحبـــا بــك</strong></h5>
        <p class="display-4"><strong>مخبز سنابل المدينة</strong></p>
        <p class="display-4">&nbsp;</p>
        <a href="#" class="btn btn-warning btn-lg float-right">تســوق الآن</a>
    </header>
    <div class="container">
        @if ( session()->has('msg') )
            <div class="alert alert-success">{{ session()->get('msg') }}</div>
        @endif
        <div class="row text-center mb-4 mt-4">
            <div class="col-md-12 p-1 mb-4">
                <h2>تســــوق الأن</h2>
            </div>
            @foreach ($products as $product)
                <div class="col-lg-3 col-md-6 mb-4">
                    <div class="card">
                        <img style="height: 234px" class="card-img-top hover-image" src="{{ url('/uploads') . '/' . $product->image }}" alt="">
                        <p class="card-text" style="display: none">
                            {{ $product->description }}
                        </p>
                        <div class="card-body">
                            <h5 class="card-title">{{ $product->name }}</h5>
                        </div>
                        <div class="card-footer">
                            <form action="{{ route('cart') }}" method="post">
                                @csrf
                                <input type="hidden" name="id" value="{{ $product->product_id }}">
                                <input type="hidden" name="name" value="{{ $product->name }}">
                                <input type="hidden" name="price" value="{{ $product->price }}">
                                <button type="submit" class="btn btn-primary btn-outline-dark">
                                    <i class="fa fa-cart-plus "></i> SAR {{ $product->price }}
                                </button>
                            </form>
                        </div>
                    </div>
                </div>

            @endforeach
        </div>
    </div>
@endsection
