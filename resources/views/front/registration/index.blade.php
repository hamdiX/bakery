@extends('front.layouts.master')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6" id="register">
                <div class="card">
                    <div class="card-body">
                        <h2 class="card-title text-center">انشاء حساب جديد</h2>
                        <hr>
                        @if ( $errors->any() )
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <form action="/user/register" method="post">
                            @csrf
                            <div class="form-group">
                                <label for="name">الاســم:</label>
                                <input type="text" name="name" placeholder="الاســم" id="name" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="email">البريد الألكترونى:</label>
                                <input type="text" name="email" placeholder="البريد الألكترونى" id="email" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="password">كلمة المرور:</label>
                                <input type="password" name="password" placeholder="كلمة المرور" id="password"
                                       class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="password_confirmation">تأكيد كلمة المرور:</label>
                                <input type="password" name="password_confirmation" placeholder="تأكيد كلمة المرور" id="password_confirmation" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="address">العنوان:</label>
                                <textarea name="address" placeholder="العنوان" id="address" class="form-control"></textarea>
                            </div>
                            <div class="form-group">
                                <button class="btn btn-outline-info col-md-2"> تسجيــل</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
