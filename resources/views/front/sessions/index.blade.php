@extends('front.layouts.master')

@section('content')
    <div class="container">
        <div class="row">
             <div class="col-md-1"></div>
            <div class="col-md-8" id="register">
                <div class="card">
                    <div class="card-body">
                        <h2 class="card-title">تسجيل الدخول</h2>
                        <hr>
                        @if ( $errors->any() )
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        @if ( session()->has('msg') )
                            <div class="alert alert-success">{{ session()->get('msg') }}</div>
                        @endif
                        <form action="/user/login" method="post">
                            @csrf
                            <div class="form-group">
                                <label for="email"> البريد الألكترونى:</label>
                                <input type="text" name="email" placeholder="Email" id="email" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="password">كلمة المرور:</label>
                                <input type="password" name="password" placeholder="Password" id="password"
                                       class="form-control">
                            </div>
                            <div class="form-group">
                                <button  class="btn btn-outline-info col-md-2"> تسجيل الدخول</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
