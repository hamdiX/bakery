@extends('admin.layouts.master')

@section('page')
    تفاصيل طلب العميل
@endsection

@section('content')

    <div class="row">

        <div class="col-md-12">
            <div class="card">
                <div class="header">
{{--                    <h4 class="title">{{ $orders[0]->user->name }} Orders Details</h4>--}}
                    <p class="category">عرض قائمة طلبات العميل</p>
                </div>
                <div class="content table-responsive table-full-width">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>رقم الطلب</th>
                            <th>اسم المنتج</th>
                            <th>الكميه</th>
                            <th>السعر الكلى </th>
                            <th>حالة الطلب</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($orders as $order)
                            <tr>

                                <td>{{ $order->order_id }}</td>

                                <td>
                                    @foreach ($order->products as $item)
                                        <table class="table table-bordered">
                                            <tr>
                                                <td>{{ $item->name }}</td>
                                            </tr>
                                        </table>
                                    @endforeach
                                </td>

                                <td>
                                    @foreach ($order->orderItems as $item)
                                        <table class="table table-bordered">
                                            <tr>
                                                <td>{{ $item->quantity }}</td>
                                            </tr>
                                        </table>
                                    @endforeach
                                </td>

                                <td>
                                    @foreach ($order->orderItems as $item)
                                        <table class="table table-bordered">
                                            <tr>
                                                <td>SAR {{ $item->price }}</td>
                                            </tr>
                                        </table>
                                    @endforeach
                                </td>


                                <td>
                                    @if($order->status)
                                        <span class="label label-success">تم التأكيد</span>
                                    @else
                                        <span class="label label-warning">فى انتظار التأكيد</span>
                                    @endif
                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>


@endsection
