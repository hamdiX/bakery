@extends('admin.layouts.master')

@section('page')
    العملاء
@endsection

@section('content')

    <div class="row">

        <div class="col-md-12">
            <div class="card">
                <div class="header">
                    <h4 class="title">العملاء</h4>
                    <p class="category">قائمة بجميع العملاء المسجلين فى الموقع</p>
                </div>
                <div class="content table-responsive table-full-width">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>رقم العميل</th>
                            <th>الاسم</th>
                            <th>البريد الألكترونى</th>
{{--                            <th>تاريخ التسجيل</th>--}}
                            <th>العمليات</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($users as $user)

                            <tr>

                                <td>{{ $user->customer_id }}</td>
                                <td>{{ $user->name }}</td>
                                <td>{{ $user->email }}</td>
{{--                                <td>{{ $user->created_at->diffForHumans() }}</td>--}}
                                <td>
                                   {{ link_to_route('users.show', 'تفاصيل العميل', $user->customer_id, ['class'=>'btn btn-success btn-sm']) }}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
@endsection
