<div class="sidebar" data-background-color="white" data-active-color="danger">

    <div class="sidebar-wrapper">
        <div class="logo">
            <a href="/" class="simple-text">
                <img src="{{asset('assets/img/logo.png')}}" height="40px" width="50px">
                سنابل المدينة
            </a>
        </div>

        <ul class="nav">
            <li>
                <a href="{{ url('/admin') }}">
                    <i class="ti-panel"></i>
                    <p>الصفحة الرئيسية</p>
                </a>
            </li>
            <li>
                <a href="{{ url('/admin/products/create') }}">
                    <i class="ti-archive"></i>
                    <p>أصافة منتج </p>
                </a>
            </li>
            <li>
                <a href="{{ url('/admin/products') }}">
                    <i class="ti-view-list-alt"></i>
                    <p>عرض المنتجات </p>
                </a>
            </li>
            <li>
                <a href="{{ url('/admin/orders') }}">
                    <i class="ti-calendar"></i>
                    <p>الطلبات</p>
                </a>
            </li>
            <li>
                <a href="{{ url('/admin/users') }}">
                    <i class="fa fa-users"></i>
                    <p>العملاء</p>
                </a>
            </li>
        </ul>
    </div>
</div>
