<!doctype html>
<html lang="ar" direction="rtl" dir="rtl" style="direction: rtl">
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>

    <title>مخبز سنابل المدينة</title>

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport'/>
    <meta name="viewport" content="width=device-width"/>

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport'/>
    <meta name="viewport" content="width=device-width"/>
{{ Html::style('assets/css/bootstrap.min.css') }}
    <link rel="stylesheet"
          href="https://cdn.rtlcss.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-cSfiDrYfMj9eYCidq//oGXEkMc0vuTxHXizrMOFAaPsLt1zoCUVnSsURN+nef1lj"
          crossorigin="anonymous">
    {{ Html::style('assets/css/animate.min.css') }}
    {{ Html::style('assets/css/paper-dashboard.css') }}

    {{ Html::style('http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css') }}

    {{ Html::style('https://fonts.googleapis.com/css?family=Muli:400,300') }}

    {{ Html::style('assets/css/themify-icons.css') }}

    {{ Html::style('assets/css/style.css') }}
    <link href='https://fonts.googleapis.com/css?family=Cairo' rel='stylesheet'>
    <style>
        body {
            font-family: 'Cairo', serif !important;
        }
        .panel-heading {
            color: #333;
            background-color: #ffffff !important;
            border-color: #fff !important;
        }
        .panel-default {
            box-shadow: 1px 2px 20px #a0a0a0;
            border-radius: 18px;
        }
        .mb3 {
            margin-bottom: 30px;
        }
    </style>

</head>
<body>
<div class="wrapper">
    <div class="container" style="margin-top: 50px">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading text-center">
                        <img class="mb3" src="{{asset('assets/img/logo.png')}}" height="200px" width="200px">
                        <h3 class="panel-title mt-3 mb-4"><strong>تسجيـــــل الدخـــول</strong></h3>
                    </div>
                    <div class="panel-body">
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        @if ( session()->has('msg') )
                            <div class="alert alert-success">{{ session()->get('msg') }}</div>
                        @endif
                        <form method="post" action="/admin/login">
                            @csrf
                            <div class="form-group row mb3">
                                <label class="col-sm-2 col-form-label" for="email">البريد الألكترونى</label>
                                <div class="col-sm-9">
                                    <input type="email" name="email" id="email" placeholder="البريد الألكترونى" class="form-control  border-input">
                                </div>
                            </div>

                            <div class="form-group row mb3">
                                <label class="col-sm-2 col-form-label" for="password">كلمـــة المرور</label>
                                <div class="col-sm-9">
                                    <input type="password" name="password" id="password" placeholder="كلمـــة المرور" class="form-control  border-input">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-12 text-center">
                                    <button class="btn btn-primary" type="submit">تسجيل الدخول</button>
                                </div>

                            </div>

                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</body>

</html>
