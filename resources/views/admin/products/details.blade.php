@extends('admin.layouts.master')

@section('page')
   تفاصيل المنتج
@endsection

@section('content')

    <div class="row">

        <div class="col-md-12">
            <div class="card">
                <div class="header">
                    <h4 class="title"> تفاصيل المنتج</h4>
                    <p class="category">عرض كل التفاصيل عن المنتج</p>
                </div>
                <div class="content table-responsive table-full-width">
                    <table class="table table-striped">
                        <tbody>

                        <tr>
                            <th>رقم المنتج</th>
                            <td>{{ $product->id }}</td>
                        </tr>

                        <tr>
                            <th>اسم المنتج</th>
                            <td>{{ $product->name }}</td>
                        </tr>

                        <tr>
                            <th>الوصف</th>
                            <td>{{ $product->description }}</td>
                        </tr>

                        <tr>
                            <th>السعر</th>
                            <td>{{ $product->price }}</td>
                        </tr>

                        <tr>
                            <th>الصورة</th>
                            <td><img src="{{ url('uploads') . '/'. $product->image}}" alt="" class="img-thumbnail" style="width: 150px;"></td>
                        </tr>

                        </tbody>

                    </table>

                </div>
            </div>
        </div>
    </div>
@endsection
