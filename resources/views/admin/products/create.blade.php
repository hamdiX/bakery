@extends('admin.layouts.master')

@section('page')
   اضافه منتج جديد
@endsection

@section('content')

    <div class="row">
        <div class="col-lg-12 col-md-12">
            @include('admin.layouts.message')
            <div class="card">
                <div class="header text-center">
                    <h4 class="title">  اضافه منتج جديد</h4>
                </div>

                <div class="content">
                    {!! Form::open(['url' => 'admin/products', 'files'=>'true']) !!}
                    <div class="row">
                        <div class="col-md-8 col-lg-offset-2">
                            @include('admin.products._fields')
                            <div class="form-group">
                                {{ Form::submit('اضافة المنتج', ['class'=>'btn btn-primary']) }}
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>


@endsection
