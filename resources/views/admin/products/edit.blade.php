@extends('admin.layouts.master')

@section('page')
    تعديل المنتج
@endsection

@section('content')

    <div class="row">
        <div class="col-lg-10 col-md-10">
            @include('admin.layouts.message')
            <div class="card">
                <div class="header">
                    <h4 class="title"> تعديل المنتج</h4>
                </div>

                <div class="content">
                    <form method="post" action="{{route('products.update', $product->product_id)}}" enctype="multipart/form-data">
                        @method('PUT')
                        @csrf

{{--                    {!! Form::open(['url' => ['admin/products', ], 'files'=>'true', 'method'=>'put']) !!}--}}
                    <div class="row">
                        <div class="col-md-12">

                            @include('admin.products._fields')

                            <div class="form-group">
                                {{ Form::submit('تعديل المنتج', ['class'=>'btn btn-primary']) }}
                            </div>

                        </div>

                    </div>


                    <div class="clearfix"></div>
                    </form>
                </div>
            </div>
        </div>
    </div>


@endsection
