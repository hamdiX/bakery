@extends('admin.layouts.master')

@section('page')
      تفاصيل الطلب
@endsection


@section('content')

    <div class="row">

        <div class="col-md-12">
            <div class="card">
                <div class="header">
                    <h4 class="title">تفاصيل الطلب</h4>
                    <p class="category">تفاصيل الطلب</p>
                </div>
                <div class="content table-responsive table-full-width">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>تاريخ الطلب</th>
                            <th>العنوان</th>
                            <th>حالة الطلب</th>
                            <th>العمليات</th>
                        </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>{{ $order->id }}</td>
                                <td>{{ $order->date }}</td>
                                <td>{{ $order->address }}</td>
                                <td>
                                    @if ($order->status)
                                        <span class="label label-success">تأكيد</span>
                                    @else
                                        <span class="label label-warning">انتظار</span>
                                    @endif
                                </td>
                                <td>
                                    @if ($order->status)
                                    {{ link_to_route('order.pending','Pending', $order->order_id, ['class'=>'btn btn-warning btn-sm']) }}
                                @else
                                    {{ link_to_route('order.confirm','Confirm', $order->order_id, ['class'=>'btn btn-success btn-sm']) }}
                                @endif  </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card">
                <div class="header">
                    <h4 class="title">بيانات العميل</h4>
                    <p class="category">بيانات العميل</p>
                </div>
                <div class="content table-responsive table-full-width">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>#</th>
                            <td>{{ $order->user->customer_id }}</td>
                        </tr>
                        <tr>
                            <th>اسم العميل</th>
                            <td>{{ $order->user->name }}</td>
                        </tr>
                        <tr>
                            <th>البريد الألكترونى</th>
                            <td>{{ $order->user->email }}</td>
                        </tr>
              
                        </thead>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="card">
                <div class="header">
                    <h4 class="title">تفاصيل المنتج</h4>
                    <p class="category">تفاصيل المنتج</p>
                </div>
                <div class="content table-responsive table-full-width">
                    <table class="table table-striped">
                        <tr>
                            <th>رقم الطلب</th>
                            <th>اسم المنتج</th>
                            <th>السعر</th>
                            <th>الكمية</th>
                            <th>صورة المنتج</th>
                        </tr>
                        <tr>
                            <td>{{ $order->id }}</td>
                            <td>
                                @foreach ($order->products as $product)
                                    <table class="table">
                                        <tr>
                                            <td>{{ $product->name }}</td>
                                        </tr>
                                    </table>
                                @endforeach
                            </td>

                            <td>
                                @foreach ($order->orderItems as $item)
                                    <table class="table">
                                        <tr>
                                            <td>{{ $item->price }}</td>
                                        </tr>
                                    </table>
                                @endforeach
                            </td>
                            <td>
                                @foreach ($order->orderItems as $item)
                                    <table class="table">
                                        <tr>
                                            <td>{{ $item->quantity }}</td>
                                        </tr>
                                    </table>
                                @endforeach
                            </td>
                            <td>
                                @foreach ($order->products as $product)
                                    <table class="table">
                                        <tr>
                                            <td><img src="{{ url('uploads') . '/' . $product->image }}" alt="" style="width: 2em"></td>
                                        </tr>
                                    </table>
                                @endforeach
                            </td>
                        </tr>

                    </table>

                </div>
            </div>
        </div>
    </div>

    <a href="{{ url('/admin/orders') }}" class="btn btn-success">رجوع</a>

@endsection
