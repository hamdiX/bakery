@extends('admin.layouts.master')

@section('page')
    الطلبات
@endsection

@section('content')
    <div class="row">

        <div class="col-md-12">

            @include('admin.layouts.message')

            <div class="card">
                <div class="header">
                    <h4 class="title">الطلبات</h4>
                    <p class="category">عرض قائمة الطلبات</p>
                </div>
                <div class="content table-responsive table-full-width">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>رقم الطلب</th>
                            <th>العميل</th>
                            <th>المنتج</th>
                            <th>الكمية</th>
                            <th>حالة الطلب</th>
                            <th>العمليات</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            @foreach ($orders as $order)
                                <td>{{ $order->order_id }}</td>
                                <td>{{ $order->user->name }}</td>
                                <td>
                                @foreach ($order->products as $item)
                                    <table class="table table-bordered">
                                        <tr>
                                            <td>{{ $item->name }}</td>
                                        </tr>
                                    </table>
                                    @endforeach
                                </td>

                                <td>
                                    @foreach ($order->orderItems as $item)
                                        <table class="table table-bordered">
                                            <tr>
                                                <td>{{ $item->quantity }}</td>
                                            </tr>
                                        </table>
                                    @endforeach
                                </td>

                                <td>
                                    @if ($order->status)
                                        <span class="label label-success">تم تأكيد الطلب</span>
                                    @else
                                        <span class="label label-warning">فى انتظار التأكيد</span>
                                    @endif
                                </td>

                            <td>

                                @if ($order->status)
                                    {{ link_to_route('order.pending','قيد الانتظار', $order->order_id, ['class'=>'btn btn-warning btn-sm']) }}
                                @else
                                    {{ link_to_route('order.confirm','تأكيد', $order->order_id, ['class'=>'btn btn-success btn-sm']) }}
                                @endif

                                {{ link_to_route('orders.show','تفاصيل الطلب', $order->order_id, ['class'=>'btn btn-success btn-sm']) }}

                            </td>
                        </tr>
                        @endforeach



                        </tbody>
                    </table>

                </div>
            </div>
        </div>


    </div>
@endsection
