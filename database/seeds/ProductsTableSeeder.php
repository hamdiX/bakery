<?php

use App\Product;
use Faker\Factory;
use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();
        $path    = './public/uploads';
        $images = scandir($path);
        $files = array_diff(scandir($path), array('.', '..'));
        foreach ($files as $file) {
            Product::create([
                'name' => $faker->name,
                'price' => rand(10,999),
                'description' => 'وصف المنتج وصف المنتج',
                'image' => $file,
                'in_stock' => 10,
            ]);
        }
    }
}
