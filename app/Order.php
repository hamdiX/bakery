<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $primaryKey = 'order_id';
    protected $guarded = [];
    public $timestamps = false;
    public function user() {
        return $this->belongsTo(User::class, 'customer_id');
    }

    public function OrderItems() {
        return $this->hasMany(OrderItems::class,  'order_id');
    }

    public function products() {
        return $this->belongsToMany(Product::class,'order_items', 'order_id', 'product_id');
    }


}
